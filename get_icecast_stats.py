# -*- coding: utf-8 -*-
from xml.etree import ElementTree

import sys
import requests
import time
import socket

__author__ = 'oriolj'

"""

Each of these functions requires HTTP authentication
http://192.168.1.10:8000/admin/listmounts
http://192.168.1.10:8000/admin/listclients?mount=/

http://icecast.org/docs/icecast-2.3.0/admin-interface.html

python3 get_icecast_stats.py http://localhost:8001 admin hackme3344

requieres python 3.5+

"""

# TODO: support multiple icecasts on a server
# TODO: support for extra data of KH servers
# TODO: calc uptime in seconds with server_stat_iso8601
# TODO: Argument parser
# TODO: report avg bitrate serverd (depending on sources and listener per source)
# TODO: report max listeners supported depending on avg bitrate
# TODO: improve cli with argparse
# TODO: print one line per every source (with listeners, bytes, slow_listeners, uptime)


def get_admin_stats(host, admin_user, admin_psw):
    r = requests.get('%s/admin/stats' % host, auth=(admin_user, admin_psw))
    if r.status_code != 200:
        raise Exception()
    root = ElementTree.fromstring(r.text)
    client_connections = root.find("client_connections").text
    admin = root.find("admin").text  # Mail
    clients = int(root.find("clients").text)
    listener_connections = int(root.find("listener_connections").text)
    # global listeners can be negative on some circumstances, due to icecast bugs
    # so is better to iterate over the sources and aggregate the listeners
    global_listeners = int(root.find("listeners").text)
    source_relay_connections = int(root.find("source_relay_connections").text)
    source_total_connections = int(root.find("source_total_connections").text)
    sources = int(root.find("sources").text)
    stats = int(root.find("stats").text)
    stats_connections = int(root.find("stats_connections").text)
    connections = int(root.find("connections").text)
    source_client_connections = int(root.find("source_client_connections").text)
    file_connections = int(root.find("file_connections").text)
    host_name_icecast = root.find("host").text  # streaming.server.com
    server_id = root.find("server_id").text  # Icecast 2.4.3
    server_start =root.find("server_start").text  # Sun, 02 Dec 2018 20:26:41 +0100
    server_start_iso8601 =root.find("server_start_iso8601").text  # 2018-12-02T20:26:41+0100

    total_bytes_sent = 0
    total_bytes_read = 0
    listeners = 0
    total_slow_listeners = 0
    bitrates = []
    iterator = root.iter("source")
    for source in iterator:
        total_bytes_read += int(source.find("total_bytes_read").text)
        total_bytes_sent += int(source.find("total_bytes_sent").text)
        listeners += int(source.find("listeners").text)
        total_slow_listeners += int(source.find("slow_listeners").text)
        try:
            bitrates.append(int(source.find("bitrate").text))
        except:
            pass

    bitrate_avg = int(sum(bitrates) / len(bitrates))

    measurement = "icecast_admin_stats"
    # tags are optional in line protocol
    # For best performance you should sort tags by key before sending them to the database.
    # Valid Line Protocol with no tag set:
    # weather temperature=82 1465839830100400200
    # TODO: don't hardcode port
    tag_set = {
        # "host_name_icecast": f"{host_name_icecast}"
        "host_name_icecast": socket.gethostname()
    }

    # The field(s) for your data point. Every data point requires at least one field in Line Protocol.
    # Separate field key-value pairs with an equals sign = and no spaces:
    field_set = {
        "clients": clients,
        "client_connections": client_connections,
        "listener_connections": listener_connections,
        "total_bytes_read": total_bytes_read,
        "total_bytes_sent": total_bytes_sent,
        "listeners": listeners,
        "total_slow_listeners": total_slow_listeners,
        "sources_bitrate_avg": bitrate_avg
    }

    # optional time stamp
    # The timestamp for your data point in nanosecond-precision Unix time
    # timestamp = time.time()

    line = measurement

    for key, value in tag_set.items():
        line += ",%s=%s" % (key, value)

    line += " "

    for key, value in field_set.items():
        line += "%s=%s," % (key, value)

    if "," in line[-1]:
        line = line[0:-1]

    return line


if __name__ == "__main__":
    # host, admin, psw
    print(get_admin_stats(sys.argv[1], sys.argv[2], sys.argv[3]))
